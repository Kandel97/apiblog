# Pull base image
#tomamos una imagen de un python 
FROM python:3.10.4-slim-bullseye
# Set environment variables
#definimos variables
#desabilite: no controle el versionamiento del pip
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
# no crea una archivo pyc, esto es por defecto
ENV PYTHONDONTWRITEBYTECODE 1
#todo el buffer de python me lo muestra en la consola
ENV PYTHONUNBUFFERED 1
# Set work directory
#establecer un directorio de trabajo denominqado code
WORKDIR /code
# Install dependencies
#instalar todos los requeriments
COPY ./requirements.txt .
RUN pip install -r requirements.txt
# Copy project
#copie todos los archivos a la carpeta copy
COPY . .
